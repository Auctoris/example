# Example
This is an example of how to build Python modules with Poetry...

Remember to run `poetry install` before trying to use the package you're developing (e.g. via PyTest)
