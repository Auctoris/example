"""This is an example Python package developed using Poetry"""
from example.__main__ import about, sum, div

__all__ = ["about", "sum", "div"]
