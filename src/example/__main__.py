"""This is a simple example module"""


def about():
    """This is a simple text string for the example"""
    return "This is an example"


def sum(a, b):
    """This function will sum two numbers"""
    return a + b


def div(a, b):
    """This function will do an integer division on two numbers
    and return 0 in the event of a div by zero..."""
    if b == 0:
        return 0
    else:
        return a // b
