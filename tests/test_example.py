import example
import pytest


def test_sum():
    assert example.sum(3, 4) == 7


def test_invalid_sum():
    with pytest.raises(TypeError):
        example.sum(2, "2")


def test_div():
    assert example.div(5, 0) == 0
    assert example.div(8, 2) == 4
    assert example.div(7, 2) == 3


def test_invalid_div():
    with pytest.raises(TypeError):
        example.div(2, "a")
